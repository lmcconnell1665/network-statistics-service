import unittest
from unittest.mock import Mock


def build_time_mock():
	time_mock = Mock()
	time_mock.time.return_value = 0
	return time_mock


import sys
time_mock = build_time_mock()
sys.modules["time"] = time_mock
from NetworkStatisticsService.time_utils import TimeUtils

class TimeUtilsTest(unittest.TestCase):
	def test_get_current_timestamp_return_as_dict(self):
		expected_output = {"Timestamp": 0}
		actual_output = TimeUtils.get_current_timestamp()
		self.assertEqual(expected_output, actual_output)

	def test_get_current_timestamp_return_as_dict_different_key(self):
		expected_output = {"time": 0}
		actual_output = TimeUtils.get_current_timestamp(dict_key='time')
		self.assertEqual(expected_output, actual_output)


	def test_get_current_timestamp_return_as_number(self):
		expected_output = 0
		actual_output = TimeUtils.get_current_timestamp(return_as_dict=False)
		self.assertEqual(expected_output, actual_output)