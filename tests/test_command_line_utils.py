from unittest.mock import Mock


def build_subprocess_mock():
	subprocess_mock = Mock()
	process_mock = Mock()
	subprocess_mock.Popen.return_value = process_mock
	process_mock.communicate.return_value = (None, None)
	return subprocess_mock


import sys
subprocess_mock = build_subprocess_mock()
sys.modules["subprocess"] = subprocess_mock
from NetworkStatisticsService.command_line_utils import CommandLineUtils
import unittest


class LatencyStatisticUtilsTest(unittest.TestCase):
	def test_command_line_argument_processing_1(self):
		command = "ping www.google.com -c 5"
		CommandLineUtils.run_bash_command(command)
		actual_process_args = subprocess_mock.Popen.call_args[0][0]
		expected_process_args = ['ping', 'www.google.com', '-c', '5']
		self.assertEqual(actual_process_args, expected_process_args)

	def test_command_line_argument_processing_2(self):
		command = "speedtest --simple"
		CommandLineUtils.run_bash_command(command)
		actual_process_args = subprocess_mock.Popen.call_args[0][0]
		expected_process_args = ['speedtest', '--simple']
		self.assertEqual(actual_process_args, expected_process_args)




