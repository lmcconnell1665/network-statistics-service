from NetworkStatisticsService.speed_statistic_utils import SpeedStatisticUtils
import unittest
from NetworkStatisticsService.exceptions import SpeedStatisticsException


class SpeedStatisticUtilsTest(unittest.TestCase):

    def test_extract_speed_statistics_from_command_output_default(self):
        given_input = ['Download: 23.40 Mbit/s', 'Upload: 7.35 Mbit/s']
        expected_output = {'Download': 23.40, 'Upload': 7.35}
        actual_output = SpeedStatisticUtils.extract_speed_statistics_from_command_output(given_input)
        self.assertEqual(expected_output, actual_output)

    def test_extract_speed_statistics_from_command_output_only_one_present(self):
        given_input = ['Download: 23.40 Mbit/s']
        expected_output = {'Download': 23.40}
        actual_output = SpeedStatisticUtils.extract_speed_statistics_from_command_output(given_input)
        self.assertEqual(expected_output, actual_output)

    def test_extract_speed_statistics_from_command_output_none_present(self):
        given_input = []
        expected_output = {}
        actual_output = SpeedStatisticUtils.extract_speed_statistics_from_command_output(given_input)
        self.assertEqual(expected_output, actual_output)

    def test_extract_speed_statistics_from_command_output_zero_value(self):
        given_input = ['Download: ', 'Upload: ']
        self.assertRaises(SpeedStatisticsException, SpeedStatisticUtils.extract_speed_statistics_from_command_output, given_input)
