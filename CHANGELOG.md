
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- No release planned at the moment.

## [1.0.4] - 2020-07-31

### Added

- Added possibility to also upload a TimeToLive attribute (computed only in months) when using the provided DynamoDB Uploader. Check README.md for usage example.


## [1.0.3] - 2020-06-16

### Fixed

- DynamoDB upload failed due to an error in parsing the ping results when the network is down.

## [1.0.2] - 2020-06-09

### Fixed

- Initial version assumed that the ping command puts the packet loss info last. Now we search by keyword in order to parse the ping command on Raspbian OS.

## [1.0.1] - 2020-06-08

### Changed

- Bump down Python version to 3.5 to make it directly available to Raspbian OS.

## [1.0.0] - 2020-06-05

### Added

- Software Documentation

## [0.0.1] - 2020-05-11

### Added

- First release of the software.
 
[0.0.1]: https://pypi.org/project/network-statistics-service/0.0.1/
[1.0.0]: https://pypi.org/project/network-statistics-service/1.0.0/
[1.0.1]: https://pypi.org/project/network-statistics-service/1.0.1/
[1.0.2]: https://pypi.org/project/network-statistics-service/1.0.2/
[1.0.3]: https://pypi.org/project/network-statistics-service/1.0.3/
[1.0.4]: https://pypi.org/project/network-statistics-service/1.0.4/